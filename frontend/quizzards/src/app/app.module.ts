import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { QuizPageComponent } from './quiz-page/quiz-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { QuizResultsPageComponent } from './quiz-results-page/quiz-results-page.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { LoginPageComponent } from './user/login-page/login-page.component';
import { RegisterPageComponent } from './user/register-page/register-page.component';
import { UserModule } from './user/user.module';
import { JwtModule } from '@auth0/angular-jwt';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    QuizPageComponent,
    ProfilePageComponent,
    QuizResultsPageComponent,
    SidebarComponent,
    LeaderboardComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('access_token')
        }

      }
    })

  ],
  exports: [
    AppComponent,
    DashboardComponent,
    QuizPageComponent,
    ProfilePageComponent,
    QuizResultsPageComponent,
    SidebarComponent,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
