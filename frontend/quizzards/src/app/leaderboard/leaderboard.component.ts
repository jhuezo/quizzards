import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Leader } from '../models/leader.model';
import { RandomUser } from '../models/randomName.model';
import { LoginService } from '../services/login.service';
import { NamesService } from '../services/names.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})


export class LeaderboardComponent implements OnInit {
  loggedIn: boolean = false
  user: string = ''
  python:Leader[] = []
  java:Leader[] = []
  cpp:Leader[] = []

  // public nameList: any = ["cool_name_1",
  //                         "cool_name_2",
  //                         "even_cooler_name"];

  // public scoreList: any = [100,
  //                         80,
  //                         80];

  leaders: Leader[] = []

  constructor(public nameService: NamesService, public loginService: LoginService, public userService: UserService) {}

  ngOnInit(): void {
    this.user =  this.loginService.getUser()
    this.getUsers()
    this.loggedIn = true
  }

  getUsers() {
    this.userService.getAllUsersInfo().subscribe((res: Leader[]) => {

      this.cpp = res
      this.python = res
      this.java = res

      this.cpp.sort((a: Leader,b: Leader) => {
        return b.cpp_score - a.cpp_score
      })

      this.java.sort((a: any,b: any) => {
        return b.java_score - a.java_score
      })

      this.python.sort((a: any,b: any) => {
        return b.python_score - a.python_score
      })

    })
    return

  }

}
