import { Component, OnInit } from '@angular/core';
import { first, last } from 'rxjs/operators';
import { User } from '../models/user.model';
import { LoginService } from '../services/login.service';
import { NamesService } from '../services/names.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  pythonScore: any
  javaScore: any
  cppScore: any
  totalScore: any
  userLevel: any
  user = this.loginService.user
  randName:any = []
  constructor(public userService: UserService, public loginService: LoginService, public nameService: NamesService) { }

  ngOnInit(): void {
    this.getPythonScore()
    this.getJavaScore()
    this.getCppScore()
    this.getRandomName()

  }

  getRandomName() {
    this.nameService.getRandomNameByUsername(this.user).pipe(first()).subscribe((res: any) => {
      console.log("randddddd ", res[0])
      this.randName = res[0]
      console.log("randon name", this.randName)
    })
  }

  getUserLevel(){

    if (this.totalScore < 151){
      this.userLevel = "Intern"
      return "Intern"
    }
    if (this.totalScore > 150 && this.totalScore < 251){
      this.userLevel = "Associate"
      return "Associate"
    }
    this.userLevel = "Coding Wizard"
    return "Coding Wizard"
  }

  getBadges(){
    if (this.totalScore < 151){
      return 1
    }
    if (this.totalScore > 150 && this.totalScore < 251){
      return 2
    }
    return 3
  }

  getPythonScore() {
    this.userService.getPythonScore(this.user).subscribe((data: any) => {
      console.log("data", data)
      this.pythonScore = data[0].python_score
      if (this.totalScore == undefined){
      this.totalScore = this.pythonScore
      }
      else {
        this.totalScore = this.totalScore + this.pythonScore
      }
    })
  }

  getJavaScore() {
    this.userService.getJavaScore(this.user).subscribe((data: any) => {
      this.javaScore = data[0].java_score
      if (this.totalScore == undefined){
        this.totalScore = this.javaScore
        }
        else {
          this.totalScore = this.totalScore + this.javaScore
        }

      return this.javaScore
    })
  }

  getCppScore() {
    this.userService.getCppScore(this.user).subscribe((data: any) => {
      this.cppScore = data[0].cpp_score
      if (this.totalScore == undefined){
        this.totalScore = this.cppScore
        }
        else {
          this.totalScore = this.totalScore + this.cppScore
        }
    })
  }

}
