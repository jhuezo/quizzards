import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { NamesService } from 'src/app/services/names.service';
import { QuizQuestionsService } from 'src/app/services/quiz-questions.service';
import { UserService } from 'src/app/services/user.service';



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit{
  pythonScore: any
  javaScore: any
  cppScore: any
  totalScore: any
  userName!: string

  constructor(public loginService: LoginService, public nameService: NamesService,
  public userService: UserService, private route: Router, public quizService: QuizQuestionsService) {
    this.isLoggedIn()
   }

  ngOnInit() {
    this.getPythonScore()
    this.getJavaScore()
    this.getCppScore()
  }

  goToProfile() {
    return this.route.navigateByUrl('profile');
  }

  isLoggedIn() {
    this.userName =  this.loginService.getUser()
  }


  logout() {
    this.loginService.logoutUser()
  }

  resetQuizzes() {
    this.quizService.python = false
    this.quizService.java = false
    this.quizService.cpp = false
    this.quizService.easy = false
    this.quizService.medium = false
    this.quizService.hard = false
    this.quizService.pythonQuestions = [];
    this.quizService.javaQuestions = [];
    this.quizService.cppQuestions = [];
    this.quizService.pythonSolutions = [];
    this.quizService.javaSolutions = [];
    this.quizService.cppSolutions = [];
    this.quizService.pythonWrongSolutions = [];
    this.quizService.javaWrongSolutions = [];
    this.quizService.cppWrongSolutions = [];

    // medium
    this.quizService.pythonMedQuestions = [];
    this.quizService.javaMedQuestions = [];
    this.quizService.cppMedQuestions = [];
    this.quizService.pythonMedSolutions = [];
    this.quizService.javaMedSolutions = [];
    this.quizService.cppMedSolutions = [];
    this.quizService.pythonMedWrongSolutions = [];
    this.quizService.javaMedWrongSolutions = [];
    this.quizService.cppMedWrongSolutions = [];

    //hard
    this.quizService.pythonHardQuestions = [];
    this.quizService.javaHardQuestions = [];
    this.quizService.cppHardQuestions = [];
    this.quizService.pythonHardSolutions = [];
    this.quizService.javaHardSolutions = [];
    this.quizService.cppHardSolutions = [];
    this.quizService.pythonHardWrongSolutions = [];
    this.quizService.javaHardWrongSolutions = [];
    this.quizService.cppHardWrongSolutions = [];
  }

  getPythonScore() {
    if(this.loginService.loggedIn===true) {
      this.userService.getPythonScore(this.userName).subscribe((data: any) => {
        this.pythonScore = data[0].python_score
        if (this.totalScore == undefined){
        this.totalScore = this.pythonScore
        }
        else {
          this.totalScore = this.totalScore + this.pythonScore
        }
      })
    }

  }

  getJavaScore() {
    if(this.loginService.loggedIn===true) {
      this.userService.getJavaScore(this.userName).subscribe((data: any) => {
        this.javaScore = data[0].java_score
        if (this.totalScore == undefined){
          this.totalScore = this.javaScore
          }
          else {
            this.totalScore = this.totalScore + this.javaScore
          }
      })
    }
  }

  getCppScore() {
    if(this.loginService.loggedIn===true) {
      this.userService.getCppScore(this.userName).subscribe((data: any) => {
        this.cppScore = data[0].cpp_score
        if (this.totalScore == undefined){
          this.totalScore = this.cppScore
          }
          else {
            this.totalScore = this.totalScore + this.cppScore
          }
      })
    }
  }

}
