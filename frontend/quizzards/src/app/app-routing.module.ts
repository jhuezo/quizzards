import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginPageComponent } from './user/login-page/login-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { QuizPageComponent } from './quiz-page/quiz-page.component';
import { QuizResultsPageComponent } from './quiz-results-page/quiz-results-page.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { UserComponent } from './user/user.component';
import { RegisterPageComponent } from './user/register-page/register-page.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'home', component: DashboardComponent
  },
  {
    path: 'login', component: UserComponent, children: [{ path: '', component: LoginPageComponent}]
  },
  {
    path: 'register', component: UserComponent, children: [{ path: '', component: RegisterPageComponent}]
  },
  {
    path: 'quizzes', component:QuizPageComponent
  },
  {
    path: 'profile', component: ProfilePageComponent, canActivate: [AuthGuard]
  },
  {path: 'result', component: QuizResultsPageComponent},
  {path: 'leaderboard', component: LeaderboardComponent, canActivate: [AuthGuard]},
  // {
  //   path: '**', redirectTo: 'home', pathMatch: 'full'
  // },
  {
    path: '', redirectTo:'home', pathMatch: 'full'
  }]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
