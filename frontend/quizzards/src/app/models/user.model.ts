export interface User {
  users_name: string,
  users_password: string,
  python_score?: Number,
  java_score?: Number,
  cpp_score?: Number,
  access_token: string,
  status: string

}
