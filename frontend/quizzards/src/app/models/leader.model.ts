export interface Leader {
  users_name: string,
  randomname: string,
  python_score: number,
  java_score: number,
  cpp_score: number
}
