import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, shareReplay } from 'rxjs/operators';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {


  loginForm!: FormGroup
  submitted = false
  returnUrl: string = ''
  error: string = ''

  constructor(public loginService: LoginService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
    this.loginService.logoutUser()

  }

  get f() {
    return this.loginForm.controls
  }

  setUsername(username: string) {
    this.loginService.user = username
  }

  login() {
    this.submitted = true
    if(this.loginForm.invalid) {
      // console.log("invalid form")
      return
    }

    this.loginService.loginUser(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value).subscribe((res:any) => {
      // console.log("successful sign in!", res)
      this.loginService.setUser(res)
      this.loginService.user = this.loginForm.get('username')?.value
      this.loginService.loggedIn = true
    });

  }
}



