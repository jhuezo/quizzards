import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  registerForm!: FormGroup
  submitted: boolean = false
  constructor(private loginService: LoginService, public fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
      python_score: [0],
      java_score: [0],
      cpp_score: [0]
    });
  }

  get f() {
    return this.registerForm.controls
  }

  registerUser() {
    this.submitted = true

    return this.loginService.createUser(this.registerForm.get("username")?.value, this.registerForm.get("password")?.value, this.registerForm.get("python_score")?.value, this.registerForm.get("java_score")?.value, this.registerForm.get("cpp_score")?.value)
    .subscribe(() => {
      this.router.navigateByUrl('/login')
    })
  }

}
