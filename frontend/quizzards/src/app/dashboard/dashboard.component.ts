import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from '../models/user.model';
import { LoginService } from '../services/login.service';
import { QuizQuestionsService } from '../services/quiz-questions.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  loading: boolean = false
  users: User[] = []

  ngOnInit() {
    this.quizService.easy = false;
    this.quizService.medium = false;
    this.quizService.hard = false;
    this.grabPyQuestions()
    this.grabJavaQuestions()
    this.grabCppQuestions()
    this.grabPySolutions()
    this.grabJavaSolutions()
    this.grabCppSolutions()
    this.grabPyWrongSolutions()
    this.grabJavaWrongSolutions()
    this.grabCppWrongSolutions()
    this.getUserInfo()

  }

  constructor(public quizService: QuizQuestionsService, public loginService: LoginService) { }
  // easy?: boolean, medium?:boolean, hard?:boolean
  // language: boolean, level: boolean
    getUserInfo() {
        this.loginService.getUserInfo(this.loginService.user).subscribe((res) => {
          if(res.status = 'success') {
            // console.log("success", res)
            this.users.push(res)
          }

        })
      }

    grabPyQuestions() {
      return this.quizService.getPythonQuestions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.pythonQuestions.push(data[i])

          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.pythonMedQuestions.push(data[i])

          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.pythonHardQuestions.push(data[i])

          }
        }
      })
    }

    grabJavaQuestions() {
      return this.quizService.getJavaQuestions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.javaQuestions.push(data[i])
          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.javaMedQuestions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.javaHardQuestions.push(data[i])
          }
        }
      })
    }

    grabCppQuestions() {
      return this.quizService.getCppQuestions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.cppQuestions.push(data[i])
          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.cppMedQuestions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.cppHardQuestions.push(data[i])
          }
        }
      })
    }

    grabPySolutions() {
      return this.quizService.getPythonSolutions().subscribe((data: any) => {
        for(let i in data) {

          if(Number(i) + 1 <= 5) {
            this.quizService.pythonSolutions.push(data[i])

          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.pythonMedSolutions.push(data[i])

          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.pythonHardSolutions.push(data[i])

          }
        }

      })
    }

    grabJavaSolutions() {
      return this.quizService.getJavaSolutions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.javaSolutions.push(data[i])


          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.javaMedSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.javaHardSolutions.push(data[i])

          }
        }
      })
    }

    grabCppSolutions() {
      return this.quizService.getCppSolutions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.cppSolutions.push(data[i])

          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.cppMedSolutions.push(data[i])

          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.cppHardSolutions.push(data[i])

          }
        }

      })
    }

    grabPyWrongSolutions() {
      return this.quizService.getPythonWrongSolutions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {

            this.quizService.pythonWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.pythonMedWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.pythonHardWrongSolutions.push(data[i])
          }
        }
      })
    }

    grabJavaWrongSolutions() {
      return this.quizService.getJavaWrongSolutions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.javaWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.javaMedWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.javaHardWrongSolutions.push(data[i])
          }
        }
      })
    }

    grabCppWrongSolutions() {
      return this.quizService.getCppWrongSolutions().subscribe((data: any) => {
        for(let i in data) {
          if(Number(i) + 1 <= 5) {
            this.quizService.cppWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 5 && Number(i) <= 9) {
            this.quizService.cppMedWrongSolutions.push(data[i])
          }
          else if(Number(i) + 1 > 10 && Number(i) <= 15) {
            this.quizService.cppHardWrongSolutions.push(data[i])
          }
        }

      })
    }

}
