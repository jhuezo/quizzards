import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NamesService } from '../services/names.service';
import { QuizQuestionsService } from '../services/quiz-questions.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { UserService } from '../services/user.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-quiz-page',
  templateUrl: './quiz-page.component.html',
  styleUrls: ['./quiz-page.component.css']
})
export class QuizPageComponent implements OnInit{
  public solutions: any = [];
  public randArr: any = [];
  form!: FormGroup;
  denominator = 0;
  score = 0;
  selectedAnswers = new Map <number, string>();
  selectedSolutions = new Map <number, number>();
  correctSolutions: any = [];
  user = this.loginService.user

  constructor(public quizService: QuizQuestionsService,
              public nameService: NamesService,
              private route: Router,
              public userService: UserService,
              public loginService: LoginService) { }

  ngOnInit() {
    this.filterAnswers()
    this.saveSolutions(this.filterAnswers())
    this.randomizeSolutions()
  }

  filterAnswers() {
    let arr:any = []
    let subarr:any = []
    let j = 0
    if(this.quizService.python) {
      if(this.quizService.easy) {
        // grab the correct solution and add to array
        for(let i in this.quizService.pythonSolutions) {

          arr.push(this.quizService.pythonSolutions[i].python_solutions)

          // grab the wrong solutions and add to array
          for(let j in this.quizService.pythonWrongSolutions[i].python_wrong_solutions) {
            arr.push(this.quizService.pythonWrongSolutions[i].python_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        return subarr
      }
      else if(this.quizService.medium) {
          // grab the correct solution and add to array
        for(let i in this.quizService.pythonMedSolutions) {
          arr.push(this.quizService.pythonMedSolutions[i].python_solutions)

          // grab the wrong solutions and add to array
          for(let j in this.quizService.pythonMedWrongSolutions[i].python_wrong_solutions) {
            arr.push(this.quizService.pythonMedWrongSolutions[i].python_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        return subarr
      }
      else if(this.quizService.hard) {
          // grab the correct solution and add to array
        for(let i in this.quizService.pythonHardSolutions) {
          arr.push(this.quizService.pythonHardSolutions[i].python_solutions)

          // grab the wrong solutions and add to array
          for(let j in this.quizService.pythonHardWrongSolutions[i].python_wrong_solutions) {
            arr.push(this.quizService.pythonHardWrongSolutions[i].python_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        return subarr
      }

    }
    else if(this.quizService.java) {
      if(this.quizService.easy) {
        // grab the correct solution and add to array
        for(let i in this.quizService.javaSolutions) {
          arr.push(this.quizService.javaSolutions[i].java_solutions)

          for(let j in this.quizService.javaWrongSolutions[i].java_wrong_solutions) {

            arr.push(this.quizService.javaWrongSolutions[i].java_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
          // add the groups of solutions into one array
          return subarr
      }
      else if(this.quizService.medium) {
        // grab the correct solution and add to array
        for(let i in this.quizService.javaMedSolutions) {
          arr.push(this.quizService.javaMedSolutions[i].java_solutions)

          for(let j in this.quizService.javaMedWrongSolutions[i].java_wrong_solutions) {

            arr.push(this.quizService.javaMedWrongSolutions[i].java_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
          // add the groups of solutions into one array
          return subarr
      }
      else if(this.quizService.hard) {
        // grab the correct solution and add to array
        for(let i in this.quizService.javaHardSolutions) {
          arr.push(this.quizService.javaHardSolutions[i].java_solutions)

          for(let j in this.quizService.javaHardWrongSolutions[i].java_wrong_solutions) {

            arr.push(this.quizService.javaHardWrongSolutions[i].java_wrong_solutions[j])
          }
        }
        // separate the solutions for each question
        // each new question starts every 4 iterations
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
          // add the groups of solutions into one array
          return subarr
      }

    }
    else if(this.quizService.cpp) {
      if(this.quizService.easy) {
        // grab the correct solution and add to array
        for(let i in this.quizService.cppSolutions) {
          arr.push(this.quizService.cppSolutions[i].cpp_solutions)
          // grab the wrong solutions and add to array
          for(let j in this.quizService.cppWrongSolutions[i].cpp_wrong_solutions) {
            arr.push(this.quizService.cppWrongSolutions[i].cpp_wrong_solutions[j])
          }
        }
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        // add the groups of solutions into one array
        return subarr
      }
      else if(this.quizService.medium) {
        // grab the correct solution and add to array
        for(let i in this.quizService.cppMedSolutions) {
          arr.push(this.quizService.cppMedSolutions[i].cpp_solutions)
          // grab the wrong solutions and add to array
          for(let j in this.quizService.cppMedWrongSolutions[i].cpp_wrong_solutions) {
            arr.push(this.quizService.cppMedWrongSolutions[i].cpp_wrong_solutions[j])
          }
        }
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        // add the groups of solutions into one array
        return subarr
      }
      else if(this.quizService.hard) {
        // grab the correct solution and add to array
        for(let i in this.quizService.cppHardSolutions) {
          arr.push(this.quizService.cppHardSolutions[i].cpp_solutions)
          // grab the wrong solutions and add to array
          for(let j in this.quizService.cppHardWrongSolutions[i].cpp_wrong_solutions) {
            arr.push(this.quizService.cppHardWrongSolutions[i].cpp_wrong_solutions[j])
          }
        }
        for(let i = 4; i <= arr.length; i+=4) {
          subarr.push(arr.slice(j, i))
          j = i
        }
        // add the groups of solutions into one array
        return subarr
      }

    }
    else {
      return console.log("error in selecting a programming language")
    }
  }




  selectAnswer (question_index: number, solution_index:number, answer: string){
  //handles the user selection of a desired answer,
  //selectedAnswers is used to display selected answer to user
  //selectedSolutions is used to calculate score
    if (this.selectedAnswers.has(question_index))
    {
      this.selectedAnswers.delete(question_index)
    }
    this.selectedAnswers.set(question_index, answer);



    if (this.selectedSolutions.has(question_index))
    {
      this.selectedSolutions.delete(question_index)
    }
    this.selectedSolutions.set(question_index, solution_index);
  }


  calculateCppScore (){
    this.correctSolutions = []
    this.denominator = (this.quizService.cppQuestions.length)
    for(let i in this.quizService.cppSolutions) {
      this.correctSolutions.push(this.quizService.cppSolutions[i].cpp_solutions)
    }

    for (let obj of this.selectedAnswers)
    {
      if (this.correctSolutions.includes(obj[1]))
      this.score++
    }
    this.score = (this.score/this.denominator)*100
    this.quizService.quizScore = this.score
    this.userService.updateCppScore(this.user, this.score).subscribe()

    if (this.score > 60)
    this.quizService.quizPoints = ((this.score/100)*10)

  }


  calculateJavaScore (){
    this.correctSolutions = []
    this.denominator = (this.quizService.javaQuestions.length)
    for(let i in this.quizService.javaSolutions) {
      this.correctSolutions.push(this.quizService.javaSolutions[i].java_solutions)
    }

    for (let obj of this.selectedAnswers)
    {
      if (this.correctSolutions.includes(obj[1]))
      this.score++
    }
    this.score = (this.score/this.denominator)*100
    this.quizService.quizScore = this.score
    this.userService.updateJavaScore(this.user, this.score).subscribe()
    if (this.score > 60)
    this.quizService.quizPoints = ((this.score/100)*10)
  }

  calculatePythonScore (){
    this.correctSolutions = []
    this.denominator = (this.quizService.pythonQuestions.length)

    for(let i in this.quizService.pythonSolutions) {
      this.correctSolutions.push(this.quizService.pythonSolutions[i].python_solutions)
    }

    for (let obj of this.selectedAnswers)
    {
      if (this.correctSolutions.includes(obj[1]))
      this.score++
    }

    this.score = (this.score/this.denominator)*100
    this.quizService.quizScore = this.score
    this.userService.updatePythonScore(this.user, this.score).subscribe()
    if (this.score > 60)
    this.quizService.quizPoints = ((this.score/100)*10)
  }


  saveSolutions(arr:any = []) {
    // save the correct solutions to solutions array
    arr.filter((item: string) => {
      this.solutions.push(item[0])
  })
  }

  //randomize possible solutions
  randomizeSolutions() {
    this.filterAnswers().forEach((item:any) => {
      this.randArr.push(item.sort(() => Math.random() - 0.5))
    })
    return this.randArr
  }

  areYouSure(){
    //this method handles incomplete quizzes, the user should be warned that the quiz is not complete
    //prior to continuing
    if (this.quizService.java){
      if (this.selectedAnswers.size == this.quizService.javaQuestions.length){
        this.calculateJavaScore()
        this.route.navigate(["/result"])
      }
      else if (confirm ("You have not answered all the questions\nAre you sure you want to proceed?"))
      {
        this.calculateJavaScore()
        this.route.navigate(["/result"])
      }
    }

    else if (this.quizService.cpp){
      if (this.selectedAnswers.size == this.quizService.cppQuestions.length){
        this.calculateCppScore()
        this.route.navigate(["/result"])
      }
      else if (confirm ("You have not answered all the questions\nAre you sure you want to proceed?"))
      {
        this.calculateCppScore()
        this.route.navigate(["/result"])
      }
    }

    else if (this.quizService.python){
      if (this.selectedAnswers.size >= this.quizService.pythonQuestions.length){
        this.calculatePythonScore()
        this.route.navigate(["/result"])
      }
      else if (confirm ("You have not answered all the questions\nAre you sure you want to proceed?"))
      {
        this.calculatePythonScore()
        this.route.navigate(["/result"])
      }
    }
  }

  refreshQuizzes() {
    this.quizService.python = false
    this.quizService.java = false
    this.quizService.cpp = false
    this.quizService.easy = false
    this.quizService.medium = false
    this.quizService.hard = false
  }


  ngDestroy() {
    this.randArr = []
  }


}
