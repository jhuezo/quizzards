
import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RandomUser } from '../models/randomName.model';
import { LoginService } from '../services/login.service';
import { NamesService } from '../services/names.service';
import { QuizQuestionsService } from '../services/quiz-questions.service';



@Component({
  selector: 'app-quiz-results-page',
  templateUrl: './quiz-results-page.component.html',
  styleUrls: ['./quiz-results-page.component.css']
})
export class QuizResultsPageComponent implements OnInit {
  user!: string | undefined
constructor(public nameService: NamesService, private route: Router, public quizService: QuizQuestionsService, public loginService: LoginService) { }

ngOnInit(): void {
    this.grabRandomNames()
  }
  //must update user_score to populate dynamically as a result of quiz-page user answer selections
  user_score = this.quizService.quizScore
  points_earned = this.quizService.quizPoints

  wizard_message = ""

  //select wizard message based on score
  getWizardMessage (score: number){
    if (score > 80)
    {
      this.wizard_message = "-- \"Looks like someone has been studying!"
    }
    if (score >= 60 && score <= 80)
    {
      this.wizard_message = "-- \"Not bad I guess, at least you're pushing yourself!"
    }
    if (score < 60)
    {
      this.wizard_message = "-- \"Uh oh... Time to hit the books!"
    }

    return this.wizard_message
  }

  getPoints (){
    return this.points_earned
  }

  getUserScore (){
    return this.quizService.quizScore
  }

  // store all the random names in an array in the service
  grabRandomNames() {
    return this.nameService.getRandomNames().subscribe((data: RandomUser) => {
      this.nameService.randomNames = data;
    })
  }

  generateRandomName() {
    let randomNum = Math.floor((Math.random() * 40) + 1);
    for(let i = 0; i < this.nameService.randomNames.length; i +=1) {
      if(i === randomNum) {
        // store the random name in service
        this.nameService.rname = this.nameService.randomNames[i-1].randomname;
        console.log("rand name: ", this.nameService.rname, 'user name: ', this.loginService.user)
        this.nameService.setRandomName(this.loginService.user, this.nameService.rname).subscribe()
        return this.nameService.randomNames[i-1].randomname;
      }

    }

  }

  isLoggedIn() {
    this.loginService.getUser()
    this.user =  this.loginService.getUser()
  }

  goToLeaderboard() {
    return this.route.navigateByUrl('/leaderboard');
  }

  retakeQuiz(){
    return this.route.navigateByUrl('/quizzes')
  }

}



