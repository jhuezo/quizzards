import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { retry, catchError, map, shareReplay } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  endPoint = "http://localhost:3000";

  user!: string
  user_id!: number
  loggedIn: boolean = false

  constructor(private http: HttpClient, private router: Router) {
   }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // routes to login and register user
  createUser(users_name: string, users_password: string, python_score: string, java_score: string, cpp_score: string): Observable<User> {
    return this.http.post<User>(this.endPoint + '/register', {users_name, users_password, python_score, java_score, cpp_score}, this.httpHeader).pipe(retry(1), catchError(this.httpError))
  }

  loginUser(users_name: string, users_password: string): Observable<User> {
    return this.http.post<User>(this.endPoint + '/login', {users_name, users_password}, this.httpHeader).pipe(retry(1), catchError(this.httpError))
  }

  setUser(resp: any) {
    localStorage.setItem('access_token', resp.jwtToken)
    this.router.navigate(['/home'])
  }

  isLoggedIn() {
    // console.log("in isloggedin")
    return localStorage.getItem('access_token') != null
  }

  getUser() {
    if(this.isLoggedIn()) {
      this.loggedIn = true
      let token = localStorage.getItem('access_token')
      let jwtData = token!.split('.')[1]
      let jwtDecodedJson = window.atob(jwtData)
      let decodedData = JSON.parse(jwtDecodedJson)

      this.user_id = decodedData.user.id
      this.user = decodedData.user.username
    }
    return this.user_id, this.user

  }




  logoutUser() {
    localStorage.clear()
    // this.userSubject!.next(null!)
    this.loggedIn = false
    this.router.navigate(['/login'])
  }

  getUserInfo(username: string): Observable<any> {
    return this.http.get<any>(this.endPoint + '/user/' + username).pipe(retry(1), catchError(this.httpError))
  }

  httpError(error: { error: { message: string; }; status: any; message: any; }) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // client side error
      console.log("error on client side")
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

}

