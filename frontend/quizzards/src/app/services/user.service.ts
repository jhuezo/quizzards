import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Leader } from '../models/leader.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  endpoint = 'http://localhost:3000'

  constructor(private http: HttpClient) { }

  getAllUsersInfo(): Observable<Leader[]> {
    return this.http.get<Leader[]>(this.endpoint + '/all_users')
  }

  getUser(): Observable<User> {
    return this.http.get<User>(this.endpoint + `/user/`)
  }

  updatePythonScore(users_name: string, score: number): Observable<User> {
    return this.http.put<User>(this.endpoint + `/python_score/${users_name}`, {python_score: score})
  }

  updateJavaScore(users_name: string, score: number): Observable<User> {
    return this.http.put<User>(this.endpoint + `/java_score/${users_name}`, {java_score: score})
  }

  updateCppScore(users_name: string, score: number): Observable<User> {
    return this.http.put<User>(this.endpoint + `/cpp_score/${users_name}`, {cpp_score: score})
  }

  getPythonScore(users_name: string): Observable<User> {
    return this.http.get<User>(this.endpoint + `/python_score/${users_name}`)
  }

  getJavaScore(users_name: string): Observable<User> {
    return this.http.get<User>(this.endpoint + `/java_score/${users_name}`)
  }

  getCppScore(users_name: string): Observable<User> {
    return this.http.get<User>(this.endpoint + `/cpp_score/${users_name}`)
  }

}
