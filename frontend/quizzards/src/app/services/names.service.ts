import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Leader } from '../models/leader.model';
import { RandomUser } from '../models/randomName.model';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class NamesService {
  endPoint = 'http://localhost:3000'
  // Once populated, you can use these values throughout different components
  public ogName: string = 'Guest User';
  public loggedInUser: string = this.loginService.user
  public randomNames: any = [];
  public rname: string = '';
  public completedQuiz: boolean = false;

  constructor(private http: HttpClient, public loginService: LoginService) { }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // api call to get the random names
  getRandomNames(): Observable<RandomUser> {
    return this.http.get<RandomUser>(this.endPoint + '/randomName', this.httpHeader);
  }

  setRandomName(users_name: string, random_name: string): Observable<Leader> {
    return this.http.put<Leader>(`${this.endPoint}/random_name/${users_name}`, {randomname: random_name}, this.httpHeader)
  }

  getRandomNameByUsername(users_name: string): Observable<Leader> {
    return this.http.get<Leader>(`${this.endPoint}/randomName/${users_name}`, this.httpHeader)
  }
}
