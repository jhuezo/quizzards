import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { PythonQuestions } from '../models/pythonQuestions.model';
import { CppQuestions } from '../models/cppQuestions.model';
import { JavaQuestions } from '../models/javaQuestions.model';
import { PythonSolutions } from '../models/pythonSolutions.model';
import { JavaSolutions } from '../models/javaSolutions.model';
import { CppSolutions } from '../models/cppSolutions.model';

@Injectable({
  providedIn: 'root'
})
export class QuizQuestionsService {
  // easy
  pythonQuestions: any = [];
  javaQuestions: any = [];
  cppQuestions: any = [];
  pythonSolutions: any = [];
  javaSolutions: any = [];
  cppSolutions: any = [];
  pythonWrongSolutions: any = [];
  javaWrongSolutions: any = [];
  cppWrongSolutions: any = [];

  // medium
  pythonMedQuestions: any = [];
  javaMedQuestions: any = [];
  cppMedQuestions: any = [];
  pythonMedSolutions: any = [];
  javaMedSolutions: any = [];
  cppMedSolutions: any = [];
  pythonMedWrongSolutions: any = [];
  javaMedWrongSolutions: any = [];
  cppMedWrongSolutions: any = [];

  //hard
  pythonHardQuestions: any = [];
  javaHardQuestions: any = [];
  cppHardQuestions: any = [];
  pythonHardSolutions: any = [];
  javaHardSolutions: any = [];
  cppHardSolutions: any = [];
  pythonHardWrongSolutions: any = [];
  javaHardWrongSolutions: any = [];
  cppHardWrongSolutions: any = [];

  python: boolean = false;
  java: boolean = false;
  cpp: boolean = false;
  easy: boolean = false;
  medium: boolean = false;
  hard: boolean = false;
  quizScore: number = 0;
  quizPoints: number = 0;

  endPoint = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getPythonQuestions(): Observable<PythonQuestions> {
    return this.http.get<PythonQuestions>(this.endPoint + '/python_questions', this.httpHeader)
  }

  getJavaQuestions(): Observable<JavaQuestions> {
    return this.http.get<JavaQuestions>(this.endPoint + '/java_questions', this.httpHeader)
  }

  getCppQuestions(): Observable<CppQuestions> {
    return this.http.get<CppQuestions>(this.endPoint + '/cpp_questions', this.httpHeader)
  }

  getPythonSolutions(): Observable<PythonSolutions> {
    return this.http.get<PythonSolutions>(this.endPoint + '/python_solutions', this.httpHeader)
  }

  getJavaSolutions(): Observable<JavaSolutions> {
    return this.http.get<JavaSolutions>(this.endPoint + '/java_solutions', this.httpHeader)
  }

  getCppSolutions(): Observable<CppSolutions> {
    return this.http.get<CppSolutions>(this.endPoint + '/cpp_solutions', this.httpHeader)
  }

  getPythonWrongSolutions(): Observable<any> {
    return this.http.get<any>(this.endPoint + '/python_wrong_solutions', this.httpHeader)
  }

  getJavaWrongSolutions(): Observable<any> {
    return this.http.get<any>(this.endPoint + '/java_wrong_solutions', this.httpHeader)
  }

  getCppWrongSolutions(): Observable<any> {
    return this.http.get<any>(this.endPoint + '/cpp_wrong_solutions', this.httpHeader)
  }

}
