import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import jwtGenerator from './jwtGenerator';
import { Pool } from 'pg';

/* connect to database */
const pool = new Pool({
    user: "quizzard",
    host: "localhost",
    database: "quizzards_database",
    password: "abc123",
    port: 5432
});


const register = async (req: Request, res: Response) => {
        // console.log("body ", req.body)
    
    try {
        const { users_name, users_password, python_score, java_score, cpp_score } = req.body;

        // console.log("user ", users_name)


        // veryfy if the user exists already
        let user = await pool.query("SELECT * from users WHERE users_name = $1",
            [users_name]);
           

        if(user.rows.length > 0) {
            return res.status(401).json("User already exists. Try Another name.");
        }

        let salt = await bcrypt.genSalt();
        let hashedPassword = await bcrypt.hash(users_password, salt);

        let newUser = await pool.query(
            "INSERT INTO users(users_name, users_password, python_score, java_score, cpp_score) VALUES ($1, $2, $3, $4, $5) RETURNING *",
            [users_name, hashedPassword, python_score, java_score, cpp_score]);

      
        const jwtToken = jwtGenerator.jwtGenerator(newUser.rows[0].users_id, users_name);
        return res.status(200).json({ jwtToken })
    } catch (error) {
   
        res.status(500).send("Server error");
    }
};

export = { 
    register
};