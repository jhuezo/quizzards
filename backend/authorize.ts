import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import jwtSecret from '../config'

const authorize = async (req: Request, res: Response, next: any) => {
    const header = req.headers.authorization;

    // verify token
    if(header) {
        const token = header.split(' ')[1];

        jwt.verify(token, jwtSecret.config.secret, (error, user) => {
            if(error) {
                console.log(error)
                return res.sendStatus(403).json("Token is not valid");
            }
            user!.token = token;
            next();
        });    
    }  
}

const verify = (req: Request, res: Response) => {
    try {
        res.json(true);
    } catch (error) {
        console.log(error)
        res.status(500).json("Server error");
    }
};

export = {
    authorize, 
    verify
}