import * as express from 'express';
import db from "../database/queries";
import register from './register';
import login from './login'
import verify  from './login';
import authorize from './authorize';

const app = express.Router();

export { app as routes};
// register for app
app.post('/register', register.register)

//login to app
app.post('/login', login.login)

// veryfy route authority
app.post('/verify', authorize.authorize, verify.login)

// Read all python questions
app.get('/python_questions', db.getPythonQuestions);

// Read all java questions
app.get('/java_questions', db.getJavaQuestions);

// Read all cpp questions
app.get('/cpp_questions', db.getCppQuestions);

// create a user
app.post('/new_user', db.createUser);

// get random name
app.get('/randomName', db.getRandomName);

// set random name by user_name
app.put('/random_name/:users_name', db.setRandomName)

// get random name by users_name
app.get('/randomname/:users_name', db.getRandomNameByUsername)

// Read all python solutions
app.get('/python_solutions', db.getPythonSolutions)

// Read all java solutions
app.get('/java_solutions', db.getJavaSolutions)

// Read all cpp solutions
app.get('/cpp_solutions', db.getCppSolutions)

// Read all wrong python solutions
app.get('/python_wrong_solutions', db.getPythonWrongSolutions)

// Read all java solutions
app.get('/java_wrong_solutions', db.getJavaWrongSolutions)

// Read all cpp solutions
app.get('/cpp_wrong_solutions', db.getCppWrongSolutions)

// Read all user names
app.get('/all_users', db.getUsers)

// Read user by username
app.get('/user/:users_name', db.getUserByName)

// Read users python score by their name
app.get('/python_score/:users_name', db.usersPythonScoreByName)

// Read users python score by their name
app.get('/java_score/:users_name', db.usersJavaScoreByName)

// Read users python score by their name
app.get('/cpp_score/:users_name', db.usersCppScoreByName)

// Update users python score
app.put('/python_score/:users_name', db.updateUsersPythonScoreByName)

// Update users python score
app.put('/java_score/:users_name', db.updateUsersJavaScoreByName)

// Update users python score
app.put('/cpp_score/:users_name', db.updateUsersCppScoreByName)


//     getPythonSolutionById,

//     getJavaSolutionById,

//     getCppSolutionById,

