import jwt from 'jsonwebtoken';
import jwtSecret from '../config'

const jwtGenerator = (users_id: Number, users_name: string) => {
    
    const payload = {
        user: {
            id: users_id,
            username: users_name
        },
    };
    return jwt.sign(payload, jwtSecret.config.secret, {expiresIn: '3hr'});
}

export = {
    jwtGenerator
}