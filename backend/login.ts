import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import jwtGenerator from './jwtGenerator';
import { Pool } from 'pg';
import authorize from './authorize'

/* connect to database */
const pool = new Pool({
    user: "quizzard",
    host: "localhost",
    database: "quizzards_database",
    password: "abc123",
    port: 5432
});

const login =  async (req: Request, res: Response) => {
    
    
    const { users_name, users_password } = req.body
    

    try {
        // validate that username is corrct
        const user = await pool.query(
            "SELECT * FROM users WHERE users_name = $1",
            [users_name]
        );
        // console.log("user ", user)

        if (user.rows.length < 0) {
            return res.status(401).json("Invalid username. Try again");
        }


      
        // validate that password is correct
        const validPass = await bcrypt.compare(users_password, user.rows[0].users_password).catch((err) => console.log("err in pass ",err))

        if(!validPass) { 
            res.status(401).json("Invalid password. Try again");
            return
        }
      
        const jwtToken = jwtGenerator.jwtGenerator(user.rows[0].users_id, users_name);
      
        return res.cookie("SESSIONID", jwtToken, {httpOnly:true, secure:true}).send({jwtToken})
    } catch (error) {
        console.log("this the error ", error);
        res.status(500).json("Server error");
    }
};

export = {
    login
}
