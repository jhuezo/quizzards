import express, { Router } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { routes } from './routes';


const app = express();
const PORT = 3000;
// app.use('/', require('./register'));
// app.use('/', require('./login'));

let corsOption = {
    origin: 'http://localhost:4200'
}
app.use(cors(corsOption));

app.use(bodyParser.json());
app.use(express.json());
app.use('/', routes);


/* Connect to the server */
app.listen(PORT, () => {
    console.log(`The server is listening on port ${PORT}.`);
});