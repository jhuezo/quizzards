import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import {Pool} from "pg";

/* connect to database */
const pool = new Pool({
    user: "quizzard",
    host: "localhost",
    database: "quizzards_database",
    password: "abc123",
    port: 5432
});

/* Get all questions and solutions by language */
const getPythonQuestions = (req: Request, res: Response) => {
    pool.query(
        "SELECT python_questions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getJavaQuestions = (req: Request, res: Response) => {
    pool.query(
        "SELECT java_questions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getCppQuestions = (req: Request, res: Response) => {
    pool.query(
        "SELECT cpp_questions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getPythonSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT python_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getJavaSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT java_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getCppSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT cpp_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getPythonWrongSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT python_wrong_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getJavaWrongSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT java_wrong_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getCppWrongSolutions = (req: Request, res: Response) => {
    pool.query(
        "SELECT cpp_wrong_solutions FROM questions ORDER BY questions_id ASC",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

/* Get specific solution by language */

const getPythonSolutionById = (req: Request, res: Response) => {
    const questions_id = req.params.questions_id;
    pool.query(
        "SELECT python_solutions FROM questions WHERE questions_id = $1",
        [questions_id],
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getJavaSolutionById = (req: Request, res: Response) => {
    const questions_id = req.params.questions_id;
    pool.query(
        "SELECT java_solutions FROM questions WHERE questions_id = $1",
        [questions_id],
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getCppSolutionById = (req: Request, res: Response) => {
    const questions_id = req.params.questions_id;
    pool.query(
        "SELECT cpp_solutions FROM questions WHERE questions_id = $1",
        [questions_id],
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

/* Adding new users */

const createUser = async (req: Request, res: Response) => {
    try {
        const { users_name, python_score, java_score, cpp_score } = req.body
        const salt = await bcrypt.genSalt();
        const users_password = await bcrypt.hash(req.body.users_password, salt);
        pool.query(
        "INSERT INTO users(users_name, users_password, python_score, java_score, cpp_score) VALUES ($1, $2, $3, $4, $5)",
        [users_name, users_password, python_score, java_score, cpp_score],
        (error, results) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
        )
    }
    catch{
        console.log("error occured in creating a new user");
    } 
}

/* Get users */

const getUsers = (req: Request, res: Response) => {
        pool.query(
        "SELECT * FROM users",
        (error: any, results: { rows: any; }) => {
            if (error) {
                throw error;
            }
            res.status(200).json(results.rows);
        } 
    )
};

const getUserByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name
    pool.query(
    "SELECT * FROM users WHERE users_name = $1;",
    [users_name],
    (error: any, results: { rows: any; }) => {
        if (error) {
            throw error;
        }
        res.status(200).json(results.rows);
    } 
)
};

const getRandomName = (req: Request, res: Response) => {
    pool.query(
        "SELECT * FROM randomNames",
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows)
        }
    )
}

const setRandomName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    const randomname = req.body.randomname
    pool.query(
        "UPDATE users SET randomname = $1 WHERE users_name = $2",
        [randomname, users_name],
        (error, results) => {
            if(error) {
                throw error
            }
            res.status(200).json(results.rows)
        }
    )
}

const getRandomNameByUsername= (req: Request, res: Response) => {
    const users_name = req.params.users_name
    pool.query('SELECT randomname FROM users WHERE users_name = $1',
    [users_name],
    (error, results) => {
        if(error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

const usersPythonScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    pool.query(
        "SELECT python_score FROM users WHERE users_name = $1",
        [users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

const usersJavaScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    pool.query(
        "SELECT java_score FROM users WHERE users_name = $1",
        [users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

const usersCppScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    pool.query(
        "SELECT cpp_score FROM users WHERE users_name = $1",
        [users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

const updateUsersPythonScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    const python_score = req.body.python_score
    pool.query(
        "UPDATE users SET python_score = $1 WHERE users_name = $2",
        [python_score, users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

const updateUsersJavaScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    const java_score = req.body.java_score
    pool.query(
        "UPDATE users SET java_score = $1 WHERE users_name = $2",
        [java_score, users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

const updateUsersCppScoreByName = (req: Request, res: Response) => {
    const users_name = req.params.users_name;
    const cpp_score = req.body.cpp_score
    pool.query(
        "UPDATE users SET cpp_score = $1 WHERE users_name = $2",
        [cpp_score, users_name],
        (error, results) => {
            if(error) {
                throw error;
            }
            res.status(200).json(results.rows);
        }
    )
};

export = {
    getPythonQuestions,
    getPythonSolutions,
    getPythonSolutionById,
    getJavaQuestions,
    getJavaSolutions,
    getJavaSolutionById,
    getCppQuestions,
    getCppSolutions,
    getCppSolutionById,
    getPythonWrongSolutions,
    getJavaWrongSolutions,
    getCppWrongSolutions,
    getRandomName,
    setRandomName,
    getRandomNameByUsername,
    getUsers,
    getUserByName,
    createUser,
    usersPythonScoreByName,
    usersJavaScoreByName,
    usersCppScoreByName,
    updateUsersPythonScoreByName,
    updateUsersJavaScoreByName,
    updateUsersCppScoreByName
}

