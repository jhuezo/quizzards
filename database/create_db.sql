DROP DATABASE IF EXISTS quizzards_database;

CREATE USER quizzard WITH PASSWORD 'abc123' CREATEDB;
GRANT pg_read_server_files, pg_write_server_files to quizzard;

CREATE DATABASE quizzards_database WITH OWNER = quizzard;

--\c into quizzards_database
