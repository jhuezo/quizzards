CREATE TABLE IF NOT EXISTS questions (
    questions_id serial PRIMARY KEY,
    python_questions TEXT,
    python_solutions TEXT,
    python_wrong_solutions TEXT[],
    java_questions TEXT,
    java_solutions TEXT,
    java_wrong_solutions TEXT[],
    cpp_questions TEXT,
    cpp_solutions TEXT,
    cpp_wrong_solutions TEXT[]
);