CREATE EXTENSION IF NOT EXISTS pgcrypto; -- this will encrypt passwords

CREATE TABLE IF NOT EXISTS users (
    users_id serial PRIMARY KEY,
    users_name VARCHAR(80),
    randomName VARCHAR(80),
    users_password VARCHAR(255),
    python_score INT,
    java_score INT,
    cpp_score INT
);
