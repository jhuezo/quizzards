\copy questions(questions_id, python_questions, python_solutions, python_wrong_solutions, java_questions, java_solutions, java_wrong_solutions, cpp_questions, cpp_solutions, cpp_wrong_solutions) FROM 'questions.csv' delimiter ',' CSV HEADER;
\copy users(users_id, users_name, users_password, python_score, java_score, cpp_score) FROM 'users.csv' delimiter ',' CSV HEADER;
\copy randomNames(id, randomName) FROM 'randomNames.csv' delimiter ',' CSV HEADER;
