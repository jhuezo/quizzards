# Quizzards

## Database
    1. From the root folder(quizzards), run the command 'cd database' to enter the database folder.
    2. Then run the command 'npm run build' in the terminal. This will prompt you to enter the password for the postgres user when you installed postgreSQL. Hit enter.
    3. You will see the database be created with a 'GRANT ROLE' and 'CREATE DATABASE' output.
    4. Then you will be prompted to enter the password for user quizzard.
    5. You will see 'CREATE TABLE', 'CREATE EXTENSION', and 'CREATE TABLE' output.
    6. Enter the password for user quizzard one last time.
    7. Lastly, you will see 'COPY 1' and 'COPY 5' display.

## Backend
    1. From the root folder(quizzards), run the command 'cd backend' to enter the backend folder.
    2. Then run the command 'npm install' to install all the dependencies needed for the backend to run.
    3. Once everything is installed run the command 'npm run serve' and you will see console output.
    4. From here open your browser and go to 'http://localhost:3000' since that is the port we are listening on.


## Frontend
    1. From the root folder(quizzards), run the command 'cd frontend' to enter the frontend folder. 
    2. Then 'cd quizzards' to enter the quizzards frontend project.
    3. Run the command 'npm install' to download all the dependencies needed to run the frontend framework.
    4. Once everything installs, run the command 'ng serve' and wait until it all builds then open your browser and go to 'http://localhost:4200' since angular's default port is 4200.
